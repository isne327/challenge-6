#include<iostream>
#include"fraction.h"
using namespace std;

int main()
{
	
	Fraction frac1,frac2,ans;
	cout << "Enter the first fraction Ex 1 2  = 1/2 : ";
	cin >> frac1;
	cout << "Enter the second fraction Ex 1 2  = 1/2 : ";
	cin >> frac2;

	//Display both Fractions
	cout << "Fraction 1 is ";
	cout << frac1 << endl;
	cout << "Fraction 2 is ";
	cout << frac2 << endl;

	//Display Fraction1 + Fraction2
	cout << "Fraction 1 + Fraction 2 = ";
	ans = frac1 + frac2;
	ans.cut();
	cout << ans << endl;

	//Display Fraction1 - Fraction2
	cout << "Fraction 1 + Fraction 2 = ";
	ans = frac1 - frac2;
	ans.cut();
	cout << ans << endl;

	//Display Fraction1 * Fraction2
	cout << "Fraction 1 * Fraction 2 = ";
	ans = frac1 * frac2;
	ans.cut();
	cout << ans << endl;

	//Display Fraction1 / Fraction2
	cout << "Fraction 1 / Fraction 2 = ";
	ans = frac1 / frac2;
	ans.cut();
	cout << ans << endl;

	//Display Fraction1 ++
	cout << "Fraction 1 + 1 = ";
	ans = ++frac1;
	ans.cut();
	cout << ans << endl;

	//Display Fraction2 ++
	cout << "Fraction 2 + 1 = ";
	ans = ++frac2;
	ans.cut();
	cout << ans << endl;

	//Display Fraction1 --
	cout << "Fraction 1 - 1 = ";
	ans = --frac1;
	ans.cut();
	cout << ans << endl;

	//Display Fraction2 --
	cout << "Fraction 2 - 1 = ";
	ans = --frac2;
	ans.cut();
	cout << ans << endl;

	//Compare if Fraction 1 == Fraction 2
	cout << "Fraction 1 = Fraction 2 : ";
	if (frac1 == frac2)
	{
		cout << "TRUE" << endl;
	}
	else cout << "FALSE" << endl;

	//Compare if Fraction 1 != Fraction 2
	cout << "Fraction 1 != Fraction 2 : ";
	if (frac1 != frac2)
	{
		cout << "TRUE" << endl;
	}
	else cout << "FALSE" << endl;

	//Compare if Fraction 1 <= Fraction 2
	cout << "Fraction 1 <= Fraction 2 : ";
	if (frac1 <= frac2)
	{
		cout << "TRUE" << endl;
	}
	else cout << "FALSE" << endl;

	//Compare if Fraction 1 >= Fraction 2
	cout << "Fraction 1 >= Fraction 2 : ";
	if (frac1 >= frac2)
	{
		cout << "TRUE" << endl;
	}
	else cout << "FALSE" << endl;

	//Compare if Fraction 1 < Fraction 2
	cout << "Fraction 1 < Fraction 2 : ";
	if (frac1 < frac2)
	{
		cout << "TRUE" << endl;
	}
	else cout << "FALSE" << endl;

	//Compare if Fraction 1 > Fraction 2
	cout << "Fraction 1 > Fraction 2 : ";
	if (frac1 > frac2)
	{
		cout << "TRUE" << endl;
	}
	else cout << "FALSE" << endl;
			
}
