#include<iostream>

using namespace std;

class Fraction
{
private:
	int num;
	int denom;

public:
	friend istream &operator >> (istream &input, Fraction &frac); //Input Fraction With >> Operator
	friend ostream &operator << (ostream &output, Fraction &frac);//Output the Fraction << Operator
	int Fraction::GetNum(); //Accessor for numerator
	int Fraction::GetDenom(); //Accessor for denomerator
	friend Fraction operator +(Fraction& frac1, Fraction& frac2); //Add Fraction with + Operator
	friend Fraction operator -(Fraction &frac1, Fraction &frac2); //Minus Fraction with - Operator
	friend Fraction operator *(Fraction &frac1, Fraction &frac2); //Multiply Fraction with * operation
	friend Fraction operator /(Fraction &frac1, Fraction &frac2); //Divide Fration with / Operation
	friend Fraction operator ++(Fraction &frac); //Add 1 to Fraction with ++ Operator
	friend Fraction operator --(Fraction &frac); //Subtract 1 from the Fraction with -- Operator 
	friend bool operator ==(Fraction &frac1, Fraction &frac2); //Compare Function with == Operator
	friend bool operator !=(Fraction &frac1, Fraction &frac2); //Compare Function with != Operator
	friend bool operator <=(Fraction &frac1, Fraction &frac2); //Compare Function with <= Operator
	friend bool operator >=(Fraction &frac1, Fraction &frac2); //Compare Function with >= Operator
	friend bool operator <(Fraction &frac1, Fraction &frac2); //Compare Function with < Operator
	friend bool operator >(Fraction &frac1, Fraction &frac2); //Compare Function with > Operator
	void Fraction::cut(); //Reduce Fraction to the lowest form
};