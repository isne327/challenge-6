#include "fraction.h"
	istream &operator >> (istream &input, Fraction &frac)
	{
		input >> frac.num >> frac.denom;
		return input;
	}

	ostream &operator << (ostream &output, Fraction &frac)
	{
		output << frac.num <<"/"<< frac.denom;
		return output;
	}

	int Fraction::GetNum()
	{
		return num;
	}

	int Fraction::GetDenom()
	{
		return denom;
	}

	Fraction operator +(Fraction& frac1, Fraction& frac2) //Overloading Operator "+"
	{
		Fraction sum;
		sum.num = (frac1.num * frac2.denom) + (frac2.num * frac1.denom);
		sum.denom = (frac1.denom * frac2.denom);
		return sum;
	}

	Fraction operator -(Fraction &frac1, Fraction &frac2) //Overloading Operator "-"
	{
		Fraction diff;
		diff.num = (frac1.num * frac2.denom) - (frac2.num * frac1.denom);
		diff.denom = (frac1.denom * frac2.denom);
		return diff;
	}

	Fraction operator *(Fraction &frac1, Fraction &frac2) //Overloading Opearator "*"
	{
		Fraction multi;
		multi.num = (frac1.num*frac2.num);
		multi.denom = (frac1.denom*frac2.denom);
		return multi;
	}

	Fraction operator /(Fraction &frac1, Fraction &frac2) //Overloading Operator "/"
	{
		Fraction divide;
		divide.num = (frac1.num*frac2.denom);
		divide.denom = (frac2.num*frac1.denom);
		return divide;
	}

	Fraction operator ++(Fraction &frac) //Overloading Operator "++"
	{
		Fraction plus;
		plus.num = frac.num + frac.denom;
		plus.denom = frac.denom;
		return plus;
	}

	Fraction operator --(Fraction &frac) //Overloading Operator "--"
	{
		Fraction minus;
		minus.num = frac.num - frac.denom;
		minus.denom = frac.denom;
		return minus;
	}

	bool operator ==(Fraction &frac1, Fraction &frac2) //Overloading Operator "=="
	{
		if (frac1.num*frac2.denom == frac2.num*frac1.denom)
		{
			return true;
		}
		else return false;
	}

	bool operator !=(Fraction &frac1, Fraction &frac2) //Overloading Operator "!="
	{
		if (frac1.num*frac2.denom != frac2.num*frac1.denom)
		{
			return true;
		}
		else return false;
	}

	bool operator <=(Fraction &frac1, Fraction &frac2) //Overloading Operator "<="
	{
		if (frac1.num*frac2.denom <= frac2.num*frac1.denom)
		{
			return true;
		}
		else return false;
	}

	bool operator >=(Fraction &frac1, Fraction &frac2) //Overloading Operator ">="
	{
		if (frac1.num*frac2.denom >= frac2.num*frac1.denom)
		{
			return true;
		}
		else return false;
	}

	bool operator <(Fraction &frac1, Fraction &frac2) //Overloading Operator "<"
	{
		if (frac1.num*frac2.denom < frac2.num*frac1.denom)
		{
			return true;
		}
		else return false;
	}

	bool operator >(Fraction &frac1, Fraction &frac2) //Overloading Operator ">"
	{
		if (frac1.num*frac2.denom > frac2.num*frac1.denom)
		{
			return true;
		}
		else return false;
	}
	
	void Fraction::cut() //Reduse Fraction to the lowest form
	{
		int high, low;
		if (num < denom)
		{
			low = num;
			high = denom;
		}
		else
		{
			low = denom;
			high = num;
		}

		for (int i = low; i > 0; i--)
		{
			if ((low%i == 0) && (high%i == 0))
			{
				num = num / i;
				denom = denom / i;
				break;
			}
		}
	}




